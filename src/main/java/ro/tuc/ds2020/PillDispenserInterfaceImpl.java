package ro.tuc.ds2020;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.services.IntakeIntervalsService;
import ro.tuc.ds2020.services.MedicationPlanService;

import java.util.UUID;

@Service
@AutoJsonRpcServiceImpl
public class PillDispenserInterfaceImpl implements PillDispenserInterface {

    MedicationPlanService medicationPlanService;
    IntakeIntervalsService intakeIntervalsService;

    @Autowired
    public PillDispenserInterfaceImpl(MedicationPlanService medicationPlanService, IntakeIntervalsService intakeIntervalsService) {
        this.medicationPlanService = medicationPlanService;
        this.intakeIntervalsService = intakeIntervalsService;
    }

    @Override
    public MedicationPlanDTO getMedicationPlan(UUID id) {
        return MedicationPlanBuilder.toDto(medicationPlanService.findMedicationPlanById(id));
    }

    @Override
    public void markAsTaken(UUID id) {
        this.intakeIntervalsService.setTaken(id);
    }

    @Override
    public void markAsNotTaken(UUID id) {
        this.intakeIntervalsService.setNotTaken(id);
    }

}
