package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class MedicationPlanService {

    MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public MedicationPlan insert(MedicationPlan medicationPlan) {
        return medicationPlanRepository.save(medicationPlan);
    }

    public MedicationPlan findMedicationPlanById(UUID id) {
        return medicationPlanRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id)
        );
    }

    public Page<MedicationPlan> findMedicationPlans(Pageable pageable) {
        return medicationPlanRepository.findAll(pageable);
    }


}