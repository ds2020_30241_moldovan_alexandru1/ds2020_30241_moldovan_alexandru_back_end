package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.DuplicateResourceException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public Page<Caregiver> findCaregivers(Pageable pageable) {
        return caregiverRepository.findAll(pageable);
    }

    public Caregiver findCaregiverById(UUID id) {
        return caregiverRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id)
        );
    }

    public Caregiver insert(Caregiver caregiver) {
        this.findOneByUsernameIgnoreCase(caregiver.getUsername()).ifPresent(caregiverDuplicate -> {
            throw new DuplicateResourceException(caregiverDuplicate.getUsername() + "already exists !");
        });
        return caregiverRepository.save(caregiver);
    }

    public void delete(UUID id) {
        caregiverRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id)
        );
        caregiverRepository.deleteById(id);
    }

    public Caregiver update(Caregiver caregiver) {
        this.findOneByUsernameIgnoreCase(caregiver.getUsername())
                .filter(existingCaregiver -> !caregiver.getId().equals(existingCaregiver.getId()))
                .ifPresent(existingCaregiver -> {
                    throw new DuplicateResourceException(existingCaregiver.getUsername() + "already exists !");
                });
        return caregiverRepository.save(caregiver);
    }

    Optional<Caregiver> findOneByUsernameIgnoreCase(String username) {
        return caregiverRepository.findOneByNameIgnoreCase(username);
    }

    public Caregiver findByUsername(String username) {
        return caregiverRepository.findByUsername(username);
    }
}
