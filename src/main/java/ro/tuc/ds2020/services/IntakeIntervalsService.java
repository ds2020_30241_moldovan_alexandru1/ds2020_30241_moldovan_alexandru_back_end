package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.IntakeIntervals;
import ro.tuc.ds2020.repositories.IntakeIntervalsRepository;

import java.util.UUID;

@Service
public class IntakeIntervalsService {
    private final IntakeIntervalsRepository intakeIntervalsRepository;

    @Autowired
    public IntakeIntervalsService(IntakeIntervalsRepository intakeIntervalsRepository) {
        this.intakeIntervalsRepository = intakeIntervalsRepository;
    }

    public IntakeIntervals insert(IntakeIntervals intakeIntervals) {
        return intakeIntervalsRepository.save(intakeIntervals);
    }

    public void setTaken(UUID id) {
        intakeIntervalsRepository.findById(id)
                .ifPresent(intakeInterval -> {
                    intakeInterval.setTaken(true);
                    intakeIntervalsRepository.save(intakeInterval);
                });

    }

    public void setNotTaken(UUID id) {
        intakeIntervalsRepository.findById(id)
                .ifPresent(intakeInterval -> {
                    intakeInterval.setTaken(false);
                    intakeIntervalsRepository.save(intakeInterval);
                });

    }


}
