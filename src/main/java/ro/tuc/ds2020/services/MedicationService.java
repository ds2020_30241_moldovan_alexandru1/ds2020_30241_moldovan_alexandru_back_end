package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.UUID;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public Page<Medication> findMedications(Pageable pageable) {
        return medicationRepository.findAll(pageable);
    }

    public Medication findMedicationById(UUID id) {
        return medicationRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id)
        );
    }

    public Medication insert(Medication medication) {
        return medicationRepository.save(medication);
    }

    public void delete(UUID id) {
        medicationRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id)
        );
        medicationRepository.deleteById(id);
    }

    public Medication update(Medication medication) {
        return medicationRepository.save(medication);
    }
}
