package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.DuplicateResourceException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Page<Person> findPersons(Pageable pageable) {
        return personRepository.findAll(pageable);
    }

    public Person findPersonById(UUID id) {
        return personRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id)
        );
    }

    public Person insert(Person person) {
        this.findOneByUsernameIgnoreCase(person.getUsername()).ifPresent(personDuplicate -> {
            throw new DuplicateResourceException(personDuplicate.getUsername() + "already exists !");
        });
        return personRepository.save(person);
    }

    public void delete(UUID id) {
        personRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id)
        );
        personRepository.deleteById(id);
    }

    public Person update(Person person) {
        this.findOneByUsernameIgnoreCase(person.getUsername())
                .filter(existingPerson -> !person.getId().equals(existingPerson.getId()))
                .ifPresent(existingPerson -> {
                    throw new DuplicateResourceException(existingPerson.getUsername() + "already exists !");
                });
        return personRepository.save(person);
    }

    Optional<Person> findOneByUsernameIgnoreCase(String username) {
        return personRepository.findOneByNameIgnoreCase(username);
    }

}
