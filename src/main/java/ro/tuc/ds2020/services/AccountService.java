package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.repositories.AccountRepository;

@Service
public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    public Account findAccountByUsername(String username) {
        return accountRepository.findAccountByUsername(username).orElseThrow(() ->
                new ResourceNotFoundException(Account.class.getSimpleName() + " with username: " + username)
        );
    }

    public Account insert(Account account) {
        return accountRepository.save(account);
    }
}
