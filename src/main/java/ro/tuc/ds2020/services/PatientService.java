package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.DuplicateResourceException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public Page<Patient> findPatients(Pageable pageable) {
        return patientRepository.findAll(pageable);
    }

    public Patient findPatientById(UUID id) {
        return patientRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id)
        );
    }

    public Patient insert(Patient patient) {
        this.findOneByUsernameIgnoreCase(patient.getUsername()).ifPresent(patientDuplicate -> {
            throw new DuplicateResourceException(patientDuplicate.getUsername() + "already exists !");
        });
        return patientRepository.save(patient);
    }

    public void delete(UUID id) {
        patientRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id)
        );
        patientRepository.deleteById(id);
    }

    public Patient update(Patient patient) {
        this.findOneByUsernameIgnoreCase(patient.getUsername())
                .filter(existingPatient -> !patient.getId().equals(existingPatient.getId()))
                .ifPresent(existingPatient -> {
                    throw new DuplicateResourceException(existingPatient.getUsername() + "already exists !");
                });
        return patientRepository.save(patient);
    }

    Optional<Patient> findOneByUsernameIgnoreCase(String username) {
        return patientRepository.findOneByNameIgnoreCase(username);
    }

    public Patient findByUsername(String username) {
        return patientRepository.findByUsername(username);
    }


}
