package ro.tuc.ds2020;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SensorController {

    private final SimpMessagingTemplate template;

    @Autowired
    public SensorController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping("/greetings")
    public void greet(String greeting) {
        this.template.convertAndSend("/topic/greetings", "Alex e cel mai tare");
    }
}
