package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public class MedicationPlanDTO {

    private UUID id;

    @NotNull
    private List<IntakeIntervalsDTO> medications;

    @NotNull
    private String period;

    public MedicationPlanDTO() {

    }

    public MedicationPlanDTO(UUID id, List<IntakeIntervalsDTO> medicationsAndIntakeIntervalsDTO, String period) {
        this.id = id;
        this.medications = medicationsAndIntakeIntervalsDTO;
        this.period = period;
    }

    public MedicationPlanDTO(List<IntakeIntervalsDTO> medicationsAndIntakeIntervalsDTO, String period) {
        this.medications = medicationsAndIntakeIntervalsDTO;
        this.period = period;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<IntakeIntervalsDTO> getMedications() {
        return medications;
    }

    public void setMedications(List<IntakeIntervalsDTO> medications) {
        this.medications = medications;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
