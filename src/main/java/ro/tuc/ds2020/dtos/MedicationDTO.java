package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class MedicationDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String sideEffects;
    @NotNull
    private String dosage;

    public MedicationDTO() {

    }

    public MedicationDTO(UUID id, @NotNull String name, @NotNull String sideEffect, @NotNull String dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffect;
        this.dosage = dosage;
    }

    public MedicationDTO(@NotNull String name, @NotNull String sideEffect, @NotNull String dosage) {
        this.name = name;
        this.sideEffects = sideEffect;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
