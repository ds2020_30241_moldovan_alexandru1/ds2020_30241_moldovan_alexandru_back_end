package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.IntakeIntervalsDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.IntakeIntervals;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.List;
import java.util.stream.Collectors;

public class MedicationPlanBuilder {
    private MedicationPlanBuilder() {

    }

    public static MedicationPlanDTO toDto(MedicationPlan medicationPlan) {
        List<IntakeIntervalsDTO> intakeIntervalsDto = medicationPlan.getMedicationsAndIntakeIntervals()
                .stream()
                .map(IntakeIntervalsBuilder::toDto)
                .collect(Collectors.toList());
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                intakeIntervalsDto,
                medicationPlan.getPeriod()
        );
    }

    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDto) {
        List<IntakeIntervals> intakeIntervalsDto = medicationPlanDto.getMedications()
                .stream()
                .map(IntakeIntervalsBuilder::toEntity)
                .collect(Collectors.toList());
        return new MedicationPlan(
                medicationPlanDto.getId(),
                intakeIntervalsDto,
                medicationPlanDto.getPeriod()
        );
    }
}
