package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.entities.Person;

public class PersonBuilder {

    private PersonBuilder() {
    }

    public static PersonDTO toDto(Person person) {
        return new PersonDTO(person.getId(),
                person.getName(),
                person.getAddress(),
                person.getAge(),
                person.getGender(),
                person.getUsername(),
                person.getPassword());
    }

    public static Person toEntity(PersonDTO personDTO) {
        return new Person(personDTO.getId(),
                personDTO.getName(),
                personDTO.getAddress(),
                personDTO.getAge(),
                personDTO.getGender(),
                personDTO.getUsername(),
                personDTO.getPassword());
    }
}
