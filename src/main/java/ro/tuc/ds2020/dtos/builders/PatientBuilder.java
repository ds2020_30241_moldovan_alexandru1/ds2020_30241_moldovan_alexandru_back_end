package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;

public class PatientBuilder {
    private PatientBuilder() {
    }

    public static PatientDTO toDto(Patient patient) {
        MedicationPlanDTO medicationPlanDTO = patient.getMedicationPlan() != null
                ? MedicationPlanBuilder.toDto(patient.getMedicationPlan())
                : null;
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getAddress(),
                patient.getAge(),
                patient.getGender(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getMedicalRecord(),
                medicationPlanDTO
        );
    }

    public static Patient toEntity(PatientDTO patientDTO) {
        MedicationPlan medicationPlan = patientDTO.getMedicationPlanDTO() != null
                ? MedicationPlanBuilder.toEntity(patientDTO.getMedicationPlanDTO())
                : null;
        return new Patient(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getAddress(),
                patientDTO.getAge(),
                patientDTO.getGender(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getMedicalRecord(),
                medicationPlan
        );
    }
}
