package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.IntakeIntervalsDTO;
import ro.tuc.ds2020.entities.IntakeIntervals;

public class IntakeIntervalsBuilder {
    private IntakeIntervalsBuilder() {

    }

    public static IntakeIntervalsDTO toDto(IntakeIntervals intakeIntervals) {
        return new IntakeIntervalsDTO(
                intakeIntervals.getId(),
                intakeIntervals.getMedication(),
                intakeIntervals.isMorning(),
                intakeIntervals.isEvening(),
                intakeIntervals.isNoon(),
                intakeIntervals.isTaken()
        );
    }

    public static IntakeIntervals toEntity(IntakeIntervalsDTO intakeIntervalsDto) {
        return new IntakeIntervals(
                intakeIntervalsDto.getId(),
                intakeIntervalsDto.getMedication(),
                intakeIntervalsDto.isMorning(),
                intakeIntervalsDto.isEvening(),
                intakeIntervalsDto.isNoon(),
                intakeIntervalsDto.isTaken()
        );
    }
}
