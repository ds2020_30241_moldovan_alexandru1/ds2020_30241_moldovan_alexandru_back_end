package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.entities.Account;

public class AccountBuilder {
    private AccountBuilder() {
    }

    public static AccountDTO toDto(Account account) {
        return new AccountDTO(account.getId(),
                account.getUsername(),
                account.getPassword(),
                account.getRole());
    }

    public static Account toEntity(AccountDTO accountDto) {
        return new Account(accountDto.getId(),
                accountDto.getUsername(),
                accountDto.getPassword(),
                accountDto.getRole());
    }
}
