package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class AccountDTO {
    private UUID id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String role;

    public AccountDTO(UUID id, @NotNull String username, @NotNull String password, @NotNull String role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public AccountDTO(@NotNull String username, @NotNull String password, @NotNull String role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public AccountDTO() {

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
