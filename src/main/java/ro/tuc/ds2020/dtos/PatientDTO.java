package ro.tuc.ds2020.dtos;

import com.sun.istack.Nullable;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class PatientDTO extends PersonDTO {
    @NotNull
    private String medicalRecord;

    @Nullable
    private MedicationPlanDTO medicationPlanDTO;

    public PatientDTO() {
    }

    public PatientDTO(String name, String address, int age, String gender, String username, String password, String medicalRecord) {
        super(name, address, age, gender, username, password);
        this.medicalRecord = medicalRecord;
    }

    public PatientDTO(UUID id, String name, String address, int age, String gender, String username, String password, String medicalRecord) {
        super(id, name, address, age, gender, username, password);
        this.medicalRecord = medicalRecord;
    }

    public PatientDTO(@NotNull String medicalRecord, MedicationPlanDTO medicationPlanDTO) {
        this.medicalRecord = medicalRecord;
        this.medicationPlanDTO = medicationPlanDTO;
    }

    public PatientDTO(String name, String address, int age, String gender, String username, String password, @NotNull String medicalRecord, MedicationPlanDTO medicationPlanDTO) {
        super(name, address, age, gender, username, password);
        this.medicalRecord = medicalRecord;
        this.medicationPlanDTO = medicationPlanDTO;
    }

    public PatientDTO(UUID id, String name, String address, int age, String gender, String username, String password, @NotNull String medicalRecord, MedicationPlanDTO medicationPlanDTO) {
        super(id, name, address, age, gender, username, password);
        this.medicalRecord = medicalRecord;
        this.medicationPlanDTO = medicationPlanDTO;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public MedicationPlanDTO getMedicationPlanDTO() {
        return medicationPlanDTO;
    }

    public void setMedicationPlanDTO(MedicationPlanDTO medicationPlanDTO) {
        this.medicationPlanDTO = medicationPlanDTO;
    }
}
