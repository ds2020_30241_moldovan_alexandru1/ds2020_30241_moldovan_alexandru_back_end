package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {
    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public Page<CaregiverDTO> getCaregivers(@PageableDefault(Integer.MAX_VALUE) Pageable pageable) {
        return caregiverService.findCaregivers(pageable).map(CaregiverBuilder::toDto);
    }

    @PostMapping()
    public CaregiverDTO insertCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        Caregiver caregiver = caregiverService.insert(CaregiverBuilder.toEntity(caregiverDTO));
        return CaregiverBuilder.toDto(caregiver);
    }

    @GetMapping(value = "/{id}")
    public CaregiverDTO getCaregiver(@PathVariable("id") UUID caregiverId) {
        return CaregiverBuilder.toDto(caregiverService.findCaregiverById(caregiverId));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteCaregiver(@PathVariable("id") UUID id) {
        this.caregiverService.delete(id);
    }

    @PutMapping
    public CaregiverDTO updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        Caregiver updatedCaregiver = caregiverService.update(CaregiverBuilder.toEntity(caregiverDTO));
        return CaregiverBuilder.toDto(updatedCaregiver);
    }

    @GetMapping("/username")
    public CaregiverDTO findByUsername(@RequestParam("username") String username) {
        return CaregiverBuilder.toDto(caregiverService.findByUsername(username));
    }

}
