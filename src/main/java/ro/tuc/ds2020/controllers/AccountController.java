package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.services.AccountService;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping()
    public AccountDTO insertAccount(@Valid @RequestBody AccountDTO accountDTO) {
        Account account = accountService.insert(AccountBuilder.toEntity(accountDTO));
        return AccountBuilder.toDto(account);
    }

    @GetMapping(value = "/{username}")
    public AccountDTO getAccount(@PathVariable("username") String username) {
        return AccountBuilder.toDto(accountService.findAccountByUsername(username));
    }

}
