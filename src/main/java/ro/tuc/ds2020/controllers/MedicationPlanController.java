package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.services.MedicationPlanService;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication-plan")
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @PostMapping()
    public MedicationPlanDTO insert(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan insertedMedPlan = medicationPlanService.insert(MedicationPlanBuilder.toEntity(medicationPlanDTO));
        return MedicationPlanBuilder.toDto(insertedMedPlan);
    }

    @GetMapping(value = "/{id}")
    public MedicationPlanDTO getMedicationPlan(@PathVariable("id") UUID id){
        return MedicationPlanBuilder.toDto(medicationPlanService.findMedicationPlanById(id));
    }

    @GetMapping()
    public Page<MedicationPlanDTO> getMedicationsPlan(@PageableDefault(Integer.MAX_VALUE) Pageable pageable) {
        return medicationPlanService.findMedicationPlans(pageable).map(MedicationPlanBuilder::toDto);
    }
}
