package ro.tuc.ds2020;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;

public final class SensorInfo implements Serializable {

    private final String activity;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private final LocalDateTime start;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private final LocalDateTime end;

    public SensorInfo(@JsonProperty("activity") String activity,
                      @JsonProperty("start") LocalDateTime start,
                      @JsonProperty("end") LocalDateTime end) {
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "Sensor info{" +
                "activity='" + activity + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}