package ro.tuc.ds2020;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
public class SensorListener {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @RabbitListener(queues = RabbitMQConfiguration.QUEUE_NAME)
    public void receiveMessage(final SensorInfo sensorInfo) throws Exception {
        long duration = Duration.between(sensorInfo.getStart(), sensorInfo.getEnd()).toHours();
        if (sensorInfo.getActivity().equals("Sleeping") && duration > 7) {
            System.out.println("Problem with " + sensorInfo.getActivity() + " time spent: " + duration);
            simpMessagingTemplate.convertAndSend("/topic/greetings", sensorInfo);
        }
        if (sensorInfo.getActivity().equals("Leaving") && duration > 5) {
            System.out.println("Problem with " + sensorInfo.getActivity() + " time spent: " + duration);
        }
        if (sensorInfo.getActivity().equals("Toileting")
                || sensorInfo.getActivity().equals("Grooming")
                || sensorInfo.getActivity().equals("Showering")
                && duration > 0.5)
            System.out.println("Problem with " + sensorInfo.getActivity() + " time spent: " + duration);
    }

}
