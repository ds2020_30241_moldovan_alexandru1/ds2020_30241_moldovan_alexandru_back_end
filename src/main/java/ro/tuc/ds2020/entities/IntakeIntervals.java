package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "intakeIntervals")
public class IntakeIntervals {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "medication_id")
    private Medication medication;

    @Column(name = "morning", nullable = false)
    private boolean morning;

    @Column(name = "noon", nullable = false)
    private boolean noon;

    @Column(name = "evening", nullable = false)
    private boolean evening;

    @Column(name = "taken", nullable = true)
    private boolean taken;

    public IntakeIntervals() {

    }

    public IntakeIntervals(UUID id, Medication medication, boolean morning, boolean noon, boolean evening) {
        this.id = id;
        this.medication = medication;
        this.morning = morning;
        this.noon = noon;
        this.evening = evening;
    }

    public IntakeIntervals(Medication medication, boolean morning, boolean noon, boolean evening) {
        this.medication = medication;
        this.morning = morning;
        this.noon = noon;
        this.evening = evening;
    }

    public IntakeIntervals(UUID id, Medication medication, boolean morning, boolean noon, boolean evening, boolean taken) {
        this.id = id;
        this.medication = medication;
        this.morning = morning;
        this.noon = noon;
        this.evening = evening;
        this.taken = taken;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public boolean isMorning() {
        return morning;
    }

    public void setMorning(boolean morning) {
        this.morning = morning;
    }

    public boolean isNoon() {
        return noon;
    }

    public void setNoon(boolean noon) {
        this.noon = noon;
    }

    public boolean isEvening() {
        return evening;
    }

    public void setEvening(boolean evening) {
        this.evening = evening;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }
}
