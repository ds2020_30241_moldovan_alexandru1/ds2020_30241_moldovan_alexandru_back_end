package ro.tuc.ds2020;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;

import java.util.UUID;

@JsonRpcService("/api")
public interface PillDispenserInterface {

    MedicationPlanDTO getMedicationPlan(@JsonRpcParam(value = "id") UUID id);

    void markAsTaken(@JsonRpcParam(value = "id") UUID id);

    void markAsNotTaken(@JsonRpcParam(value = "id") UUID id);
}